@echo off
:: BatchGotAdmin
::----------------------------------------
REM See more in https://stackoverflow.com/questions/11525056/how-to-create-a-batch-file-to-run-cmd-as-administrator
REM --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin
if '%ERRORLEVEL%' NEQ '0' (
	echo Requesting administrative priviledges...
	goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
	echo SET UAC = CreateObject^("Shell.Application"^) > "%TEMP%\getadmin.vbs"
	set params = %*:"="
	echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%TEMP%\getadmin.vbs"

	"%TEMP%\getadmin.vbs"
	del "%TEMP%\getadmin.vbs"
	exit /B

:gotAdmin
    pushd "%CD%"
	CD /D "%~dp0"
::----------------------------------------
::ENTER YOUR CODE BELOW::
ns lan