@echo off

setlocal enabledelayedexpansion
if "%1" neq "" (
  set PYENV_PATH=%1
)

if "!PYENV_PATH!" == "" (
  echo "Python Venv path unknown."
  goto end
) else (
  if "!PYENV_PATH:~-1!" neq "\" (
    set PYENV_PATH=!PYENV_PATH!\
  )

  if "!PYENV_PATH:~-8!" neq "Scripts\" (
    if "!PYENV_PATH:~-8!" neq "scripts\" (
      set PYENV_PATH=!PYENV_PATH!Scripts\
    )
  )
)

set PYENV_BATCH=%PYENV_PATH%activate.bat
echo Activating venv at !PYENV_BATCH!

rem https://stackoverflow.com/questions/33898076/passing-variable-out-of-setlocal-code
(
  endlocal
  set "ACTIVATE=%PYENV_BATCH%"
)

call %ACTIVATE%

:end
