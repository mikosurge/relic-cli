@echo off

set _RELIC_PATH=E:\tmp
set _RELIC_CLI=%_RELIC_PATH%\relic-cli
set _RELIC_VIM=%_RELIC_PATH%\rvim
set _GIT_REF=https://github.com/git-for-windows/git/releases/download/v2.28.0.windows.1/Git-2.28.0-64-bit.exe
set _PYTHON_REF=https://www.python.org/ftp/python/3.8.5/python-3.8.5.exe
set _GVIM_REF=https://ftp.nluug.nl/pub/vim/pc/gvim82.exe

set _GIT_SETUP=recall-git-setup.exe
set _PYTHON_SETUP=recall-python-setup.exe
set _GVIM_SETUP=recall-gvim-setup.exe

echo Appending User PATH...
for /f "skip=2 tokens=3*" %%a in ('reg query HKCU\Environment /v PATH') do (
  if [%%b]==[] (
    setx PATH "%%~a;%_RELIC_PATH%\rvim;%_RELIC_PATH%\relic-cli;"
  ) else (
    setx PATH "%%~a %%~b;%_RELIC_PATH%\rvim;%_RELIC_PATH%\relic-cli;"
  )
)

if not exist %_GIT_SETUP% (
  echo Will automatically download and install Git...
  call download.bat %_GIT_REF% %_GIT_SETUP%
  echo Download complete.
)
echo Installing Git...
call %_GIT_SETUP% /VERYSILENT /NORESTART /SUPPRESSMSGBOXES
echo Deleting Git Setup executable...
del %_GIT_SETUP%

if not exist %_PYTHON_SETUP% (
  echo Will automatically download and install Python...
  call download.bat %_PYTHON_REF% %_PYTHON_SETUP%
  echo Download complete. 
)
echo Installing Python...
call %_PYTHON_SETUP% /quiet PrependPath=1
echo Deleting Python Setup executable...
del %_PYTHON_SETUP%

if not exist %_GVIM_SETUP% (
  echo Will automatically download GVim...
  call download.bat %_GVIM_REF% %_GVIM_SETUP%
  echo Download complete.
)
rem call %_GVIM_SETUP% /RMOLD /D=E:\

rem Can refresh environment variables via this thread:
rem https://stackoverflow.com/questions/171588/is-there-a-command-to-refresh-environment-variables-from-the-command-prompt-in-w
rem But I'm too lazy.
start _recall.bat
pause
exit