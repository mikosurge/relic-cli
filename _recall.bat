@echo off

pushd %_RELIC_PATH%
setx PYENV_PATH %_RELIC_PATH%\renv

echo Cloning Git repositories...
call git clone https://bitbucket.org/mikosurge/relic-cli.git
call git clone https://bitbucket.org/mikosurge/rvim.git
call git clone https://bitbucket.org/mikosurge/prgcontest.git
call git clone https://bitbucket.org/mikosurge/programming-contest.git

echo Installing Python Virtualenv
call pip install virtualenv
echo Creating virtualenv...
call python -m venv %PYENV_PATH%


echo Registering git commands...
call %_RELIC_CLI%\git-setup.bat

echo Creating vimfiles/bundle in rvim...
if exist rvim (
  pushd rvim
  if not exist vimfiles (
    md vimfiles
  )
  pushd vimfiles
  if not exist bundle (
    md bundle
  )
  pushd bundle
  call git clone https://github.com/VundleVim/Vundle.vim
  rem popd of bundle
  popd
  rem popd of vimfiles
  popd
  rem popd of rvim
  popd
)


rem popd %_RELIC_PATH%
popd
 
echo Setup Done.
pause