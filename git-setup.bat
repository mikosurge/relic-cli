@echo off
git config --global alias.st status
git config --global alias.ci commit
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.last "log -1"
git config --global alias.glog "log --graph"
git config --global user.name Surge
git config --global user.email mikosurge@github.com
