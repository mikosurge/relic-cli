@echo off

set "FILE=%~dp0ns.txt"
REM set preset=0

for /F "tokens=1-3 delims==" %%a in ('Type "%FILE%"') do (
  if "%%a"=="wifi" (
    set "netswitch_wifi=%%b"
  )

  if "%%a"=="lan" (
    set "netswitch_lan=%%b"
  )
)

if "%1"=="wifi" (
  if defined netswitch_wifi (
    set _input=%netswitch_wifi%
  )
)

if "%1"=="lan" (
  if defined netswitch_lan (
    set _input=%netswitch_lan%
  )
)

setlocal ENABLEDELAYEDEXPANSION

set /a on_idx=0
set /a off_idx=0
for /F "skip=3 tokens=1,2,3,4,5 delims= " %%a in ('netsh interface show interface') do (
  if "%%a"=="Disabled" (
    if "%%e"=="" (
      set off[!off_idx!]=%%d
    )
    if NOT "%%e"=="" (
      set off[!off_idx!]=%%d %%e
    )
    set /a off_idx+=1
  )

  if "%%a"=="Enabled" (
    if "%%e"=="" (
      set on[!on_idx!]=%%d
    )
    if NOT "%%e"=="" (
      set on[!on_idx!]=%%d %%e
    )
    set /a on_idx=on_idx+1
  )
)

if NOT defined _input (
  echo Choose an interface to turn on
  set /a sel_idx=0
  :select
  if defined off[!sel_idx!] (
    set /a tmp=%sel_idx%+1
    call echo !tmp!. %%off[!sel_idx!]%%
    set /a sel_idx=sel_idx+1
    goto :select
  )
  REM goto :eof

  :input
  set /p _choice=Your choice:
  set /a _choice-=1
  if NOT defined off[!_choice!] goto :input
  set _input=!off[%_choice%]!
)

set /a sel_idx=0
:disableon
if defined on[!sel_idx!] (
  set cur=!on[%sel_idx%]!
  set /a sel_idx=sel_idx+1
  call echo Disabling interface !cur!...
  netsh interface set interface "!cur!" disabled
  set cur=
  set on[!sel_idx!]=
  goto :disableon
)

:enableoff
echo Enabling interface !_input!...
netsh interface set interface "!_input!" enabled

set /a sel_idx=0
:cleanup
if defined off[!sel_idx!] (
  set /a sel_idx=sel_idx+1
  set off[%sel_idx%]=
  goto :cleanup
)
set wifi=
set lan=
set _input=

endlocal