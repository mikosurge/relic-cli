@echo off

call powershell "Set-ExecutionPolicy -ExecutionPolicy ByPass -Scope CurrentUser; [Net.ServicePointManager]::SecurityProtocol = \"tls12\"; (New-Object System.Net.WebClient).DownloadFile(\"%1\", \"%2\")"
