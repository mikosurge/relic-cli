@echo off

REM SET PATTERN = <text in your files>
REM OUTPUT = <your output path>
SET OUTPUT = .

REM echo Getting package list
REM adb shell pm list packages | grep %PATTERN% | tr -d '\r' | gawk -F: '{print $2}' >> packages.txt

REM echo Getting path to each package
REM FOR /F "tokens=*" %%A IN (packages.txt) DO (
	REM adb shell pm path %%A | tr -d '\r' | gawk -F: '{print $2}' >> paths.txt
REM )

echo Processing each path
FOR /F "tokens=*" %%A IN (paths.txt) DO (
    FOR /F "tokens=3 delims=/" %%a IN ("%%A") DO (

		echo Processing %%a
		echo Original Path %%A
		IF NOT EXIST %OUTPUT%/%%a.apk (
			echo This apk is not exist, attempting to download
			adb shell cp %%A /sdcard/%%a.apk
			adb pull /sdcard/%%a.apk E:/grab_apk
			adb shell rm /sdcard/%%a.apk
		)
	)
)